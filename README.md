# README #

### What is this repository for? ###

* Chalenge App
* Version 1.0

### Chalenge App ###
 
  ## General overview: 
  
  * The app is a simple implementation of a webView container to display different urls.
  * In the app you can navigate into several categories via the Drawer Menu and to open items an internal view inside the app or an external browser outside the app.
  
  ##App architecture
  
  * I used MVVM architecture pattern to separate the data presentation logic(Views or UI) from the core business logic part of the application. 
  
  ##The technical process regarding the creation of the application
  
  * step1: I used to analyze the json object generally to concept the main DTO objects.
  * step 2 : I created an android application that contains only one activity ,one fragment and viewmodel and based on a navigation GRAPH,so that this application contains a drawer menu.
  * step 3:After that,I used to prepare the DTO objects.
  * Forthermore I used to prepare the network communication between the application and the server API.
  Than, I used to show my data on the view.
  * Step 4 : I handeled the user interaction with the drawer menu.
  * Step 5 : Last but not least 
   I updated the overview like colors, icons and styles ( design update ).
  * Step 6 :Finally  I created some unit tests to test some business logics methods by the application.
  
    
  ##Technology
  
  
   * Network 
   
        * Retrofit: A type-safe HTTP client for Android and Java.
        
        * Moshi: is a modern JSON library for Android and Java. It makes it easy to parse JSON into Java objects and support Polymorphism.
        
        
   * Async Task
   
        * Kotlin coroutines: A coroutine is a concurrency design pattern that you can use on Android to simplify code that executes asynchronously.  
        
        
   * Unit Test
        * Mockito: Mocking Framework.
        
        * JUnit: unit testing framework for Android and java.
        
        * kotlinx-coroutines-Test: to support Async tasks on unit test classes.
        
        
   * Eventbus: EventBus is a publish/subscribe event bus for Android and Java
   
   * Navigation Fragment: Framework from jetpack allow users to navigate across, into, and back out from the different pieces of content within your app. 