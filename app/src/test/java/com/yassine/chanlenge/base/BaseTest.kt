package com.yassine.chanlenge.base

import org.junit.Before
import org.mockito.MockitoAnnotations

open class BaseTest {

    @Before
    open fun setup() {
        MockitoAnnotations.initMocks(this)
    }
}