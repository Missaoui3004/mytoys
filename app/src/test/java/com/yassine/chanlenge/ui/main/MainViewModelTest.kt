package com.yassine.chanlenge.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.yassine.chanlenge.base.BaseTest
import com.yassine.chanlenge.base.TestCoroutineRule
import com.yassine.chanlenge.gear.repository.Repository
import com.yassine.chanlenge.network.dto.BaseEntryDto
import com.yassine.chanlenge.network.service.ApiResponse
import com.yassine.chanlenge.network.service.ResponseStatus
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.HttpException
import org.mockito.Mockito.`when` as whenever


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest : BaseTest() {


    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var viewModel: MainViewModel

    @Mock
    private lateinit var repository: Repository


    override fun setup() {
        super.setup()
        viewModel = MainViewModel()
    }


    @Test
    fun mainViewModelCanBeInvoked() {
        assertNotEquals(viewModel, null)
    }

    @Test
    fun `when fetching results ok then return a list successfully`() {
        val emptyList = arrayListOf<BaseEntryDto>()
        testCoroutineRule.runBlockingTest {

            viewModel.loadData()
            assertNotNull(viewModel.entriesListLive.value)
            assertEquals(
                ApiResponse.success(emptyList).data,
                viewModel.entriesListLive.value
            )
        }
    }


    @Test
    fun `when fetching results fails then return an error`() {
        val exception = mock(HttpException::class.java)
        testCoroutineRule.runBlockingTest {

            whenever(repository.getAllEntries()).thenAnswer {
                ApiResponse.error<String>(exception.message())
            }
            viewModel.loadData()
            assertNotNull(viewModel.entriesListLive.value)
            assertEquals(
                ApiResponse(
                    status = ResponseStatus.ERROR,
                    data = null,
                    errorMessage = null
                ),
                repository.getAllEntries()
            )
        }
    }
}