package com.yassine.chanlenge.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.yassine.chanlenge.BR
import com.yassine.chanlenge.BuildConfig
import com.yassine.chanlenge.R
import com.yassine.chanlenge.databinding.FragmentHomeBinding
import com.yassine.chanlenge.network.dto.ActionType
import com.yassine.chanlenge.ui.main.MainActivity
import com.yassine.chanlenge.utils.WebViewEvent
import kotlinx.android.synthetic.main.fragment_home.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bind(container)
        initWebView()
        return binding.root
    }

    private fun bind(container: ViewGroup?) {
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.fragment_home,
            container,
            false
        )
        binding.lifecycleOwner = this
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding.setVariable(BR.model, homeViewModel)
    }


    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        binding.webview.settings.javaScriptEnabled = true
        binding.webview.webViewClient = WebViewClient()
        binding.webview.loadUrl(BuildConfig.HOME_URL)
        homeViewModel.isLoading.set(false)
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun showWebView(event: WebViewEvent) {
        loadWebView(event)
        closeDrawer()
    }

    private fun closeDrawer() {
        (activity as MainActivity).binding.apply {
            drawerLayout.close()
        }
    }

    private fun loadWebView(event: WebViewEvent) {
        homeViewModel.isLoading.set(true)
        binding.webview.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (event.type == ActionType.LINK) {
                    return false
                } else
                    Intent(Intent.ACTION_VIEW, Uri.parse(event.url)).apply {
                        startActivity(this)
                    }
                return true
            }

            override fun onPageCommitVisible(view: WebView?, url: String?) {
                super.onPageCommitVisible(view, url)
                homeViewModel.isLoading.set(false)
            }
        }
        webview.loadUrl(event.url)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        EventBus.getDefault().register(this)
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)
    }
}