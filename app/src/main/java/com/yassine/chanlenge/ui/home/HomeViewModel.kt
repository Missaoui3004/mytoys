package com.yassine.chanlenge.ui.home

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {
    val isLoading = ObservableBoolean(true)
}