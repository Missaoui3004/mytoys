package com.yassine.chanlenge.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.yassine.chanlenge.R
import com.yassine.chanlenge.databinding.ItemExternalLinkBinding
import com.yassine.chanlenge.databinding.ItemLinkBinding
import com.yassine.chanlenge.databinding.ItemNodeBinding
import com.yassine.chanlenge.databinding.ItemSectionBinding
import com.yassine.chanlenge.ui.main.items.*

class EntriesAdapter(private val listener: OnItemClick) :
    RecyclerView.Adapter<EntriesAdapter.BaseViewHolder<DataItem>>() {

    var items: MutableList<DataItem> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    abstract class BaseViewHolder<T>(view: ViewDataBinding) :
        RecyclerView.ViewHolder(view.root) {
        abstract fun bind(item: T)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : BaseViewHolder<DataItem> {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            viewType,
            parent,
            false
        )


        return when (viewType) {
            R.layout.item_section -> SectionViewHolder(binding)
            R.layout.item_node -> NodeViewHolder(binding)
            R.layout.item_link -> LinkViewHolder(binding)
            R.layout.item_external_link -> ExternalLinkViewHolder(binding)
            else -> throw IllegalArgumentException("Invalid view type")
        }.listen { position ->
            val current = items[position]
            listener.onClick(current)
        }
    }


    override fun getItemViewType(position: Int): Int {
        return items[position].layoutResId
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: BaseViewHolder<DataItem>, position: Int) {
        val dataItem = items[position]
        holder.bind(dataItem)
    }

    inner class SectionViewHolder(private val binding: ViewDataBinding) :
        BaseViewHolder<DataItem>(binding) {
        override fun bind(item: DataItem) {
            (binding as ItemSectionBinding).item = item as SectionDataItem
        }
    }

    inner class NodeViewHolder(private val binding: ViewDataBinding) :
        BaseViewHolder<DataItem>(binding) {

        override fun bind(item: DataItem) {
            (binding as ItemNodeBinding).item = item as NodeDataItem
        }

    }

    inner class LinkViewHolder(private val binding: ViewDataBinding) :
        BaseViewHolder<DataItem>(binding) {

        override fun bind(item: DataItem) {
            (binding as ItemLinkBinding).item = item as LinkDataItem
        }
    }

    inner class ExternalLinkViewHolder(private val binding: ViewDataBinding) :
        BaseViewHolder<DataItem>(binding) {

        override fun bind(item: DataItem) {
            (binding as ItemExternalLinkBinding).item = item as ExternalLinkDataItem
        }
    }
}


fun EntriesAdapter.clear() {
    items.clear()
    notifyDataSetChanged()
}


interface OnItemClick {
    fun onClick(item: DataItem)
}


fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int) -> Unit): T {
    itemView.setOnClickListener {
        event.invoke(adapterPosition)
    }
    return this
}