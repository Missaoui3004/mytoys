package com.yassine.chanlenge.ui.main

import com.yassine.chanlenge.network.dto.*

fun EntriesAdapter.factory(
    navigationEntryList: List<BaseEntryDto>
) {

    val sectionDtoList =
        navigationEntryList.filterIsInstance<SectionEntryDto>()
    val nodeDtoList =
        navigationEntryList.filterIsInstance<NodeEntryDto>()

    val linkDtoList =
        navigationEntryList.filterIsInstance<LinkEntryDto>()

    val externalDtoList =
        navigationEntryList.filterIsInstance<ExternalLinkEntryDto>()

    clear()

    when {
        sectionDtoList.isNotEmpty() ->
            sectionDtoList.map { sectionEntryDto ->
                items.add(sectionEntryDto.map())
                items.addAll(sectionEntryDto.children.mapToItemList())
            }
        nodeDtoList.isNotEmpty() ->
            items.addAll(nodeDtoList.mapToItemList())

        linkDtoList.isNotEmpty() ->
            items.addAll(linkDtoList.mapToItemList())

        externalDtoList.isNotEmpty() ->
            items.addAll(externalDtoList.mapToItemList())
    }

    notifyDataSetChanged()
}

