package com.yassine.chanlenge.ui.main.items

import com.yassine.chanlenge.R
import com.yassine.chanlenge.network.dto.BaseEntryDto
import com.yassine.chanlenge.network.dto.LinkEntryDto
import com.yassine.chanlenge.network.dto.NodeEntryDto
import com.yassine.chanlenge.network.dto.SectionEntryDto


sealed class DataItem(val layoutResId: Int, val label: String, val children: List<DataItem>)

class SectionDataItem(label: String, children: List<DataItem>) :
    DataItem(R.layout.item_section, label, children)

class NodeDataItem(label: String, children: List<DataItem>) :
    DataItem(R.layout.item_node, label, children)

class LinkDataItem(label: String, val url: String) : DataItem(R.layout.item_link, label, listOf())

class ExternalLinkDataItem(label: String, val url: String) :
    DataItem(R.layout.item_external_link, label, listOf())


fun DataItem.mapToDto(): BaseEntryDto {
    return when (this) {
        is SectionDataItem -> SectionEntryDto(label, children.map { it.mapToDto() })
        is NodeDataItem -> NodeEntryDto(label, children.map { it.mapToDto() })
        is LinkDataItem -> LinkEntryDto(label, url)
        is ExternalLinkDataItem -> LinkEntryDto(label, url)
    }
}


