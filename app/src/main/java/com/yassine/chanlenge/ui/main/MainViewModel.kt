package com.yassine.chanlenge.ui.main

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yassine.chanlenge.gear.repository.Repository
import com.yassine.chanlenge.network.dto.ActionType
import com.yassine.chanlenge.network.dto.BaseEntryDto
import com.yassine.chanlenge.network.dto.map
import com.yassine.chanlenge.network.service.ApiService
import com.yassine.chanlenge.ui.main.items.*
import com.yassine.chanlenge.utils.NavigationEvent
import com.yassine.chanlenge.utils.WebViewEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import java.util.*


class MainViewModel : ViewModel(), OnItemClick {

    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val repository = Repository(ApiService::class.java)
    val adapter: EntriesAdapter = EntriesAdapter(this)

    val entriesListLive = MutableLiveData<List<BaseEntryDto>>(emptyList())
    val currentItem = ObservableField<DataItem>()
    private val navigationList = LinkedList<BaseEntryDto>()


    fun attacheToLifeCycle(owner: LifecycleOwner) {
        loadData()
        entriesListLive.observe(owner, { baseEntryDtoList ->
            adapter.factory(baseEntryDtoList)
        })
    }


    fun loadData() {
        uiScope.launch {
            val response = repository.getAllEntries()
            if (response.isSucces && response.data != null) {
                entriesListLive.value = response.data.navigationEntries
            } else
                Log.d("log", response.errorMessage.toString())
        }
    }

    override fun onClick(item: DataItem) {
        if (item is SectionDataItem)
            return
        else if (item is LinkDataItem || item is ExternalLinkDataItem) {

            val type: ActionType
            val url: String

            if (item is LinkDataItem) {
                type = ActionType.LINK
                url = item.url
            } else {
                url = (item as ExternalLinkDataItem).url
                type = ActionType.EXTERNAL_LINK
            }
            postEvent(WebViewEvent(url, type))
            reset()
        } else
            showItem(item)
    }

    private fun showItem(item: DataItem) {
        adapter.factory(item.children.map { it.mapToDto() })
        navigationList.addFirst(item.mapToDto())
        currentItem.set(item)
        postEvent(NavigationEvent(false))
    }

    fun onClickBack() {
        navigationList.pollFirst()
        adapter.clear()
        handleNavigation()
    }


    private fun handleNavigation() {
        if (navigationList.isNotEmpty()) {
            adapter.factory(navigationList.first().children)
            currentItem.set(navigationList.first().map())
            postEvent(NavigationEvent(true))
        } else {
            currentItem.set(null)
            adapter.factory(entriesListLive.value ?: listOf())
        }
    }


    fun reset() {
        currentItem.set(null)
        adapter.clear()
        adapter.factory(entriesListLive.value ?: listOf())
        navigationList.clear()
    }
}


fun postEvent(event: Any) {
    EventBus.getDefault().post(event)
}