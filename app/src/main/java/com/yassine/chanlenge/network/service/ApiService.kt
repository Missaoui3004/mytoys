package com.yassine.chanlenge.network.service

import com.yassine.chanlenge.network.dto.Data
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {
    @GET("mytoys/navigation")
   suspend fun getDataEntries(): Response<Data>
}