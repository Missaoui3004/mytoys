package com.yassine.chanlenge.network.dto

import com.google.gson.annotations.SerializedName
import com.squareup.moshi.Json
import com.yassine.chanlenge.ui.main.items.*


data class Data(
    @SerializedName("navigationEntries")
    val navigationEntries: List<BaseEntryDto>
)

sealed class BaseEntryDto(
    @Json(name = "type") val type: ActionType,
    val label: String,
    val children: List<BaseEntryDto>
)

class SectionEntryDto(
    label: String,
    children: List<BaseEntryDto>
) : BaseEntryDto(ActionType.SECTION, label,children)

class NodeEntryDto(
    label: String,
    children: List<BaseEntryDto>
) : BaseEntryDto(ActionType.NODE, label,children)


class LinkEntryDto(
    label: String,
    val url: String
) : BaseEntryDto(ActionType.LINK, label, listOf())


class ExternalLinkEntryDto(
    label: String,
    val url: String
) : BaseEntryDto(ActionType.EXTERNAL_LINK, label, listOf())


enum class ActionType(val columnName: String) {
    @Json(name = "section")
    SECTION("section"),

    @Json(name = "node")
    NODE("node"),

    @Json(name = "link")
    LINK("link"),

    @Json(name = "external-link")
    EXTERNAL_LINK("external-link")
}


fun SectionEntryDto.map(): SectionDataItem =
    SectionDataItem(label, children.mapToItemList())

fun NodeEntryDto.map(): NodeDataItem =
    NodeDataItem(label, children.mapToItemList())

fun LinkEntryDto.map(): LinkDataItem = LinkDataItem(label,url)
fun ExternalLinkEntryDto.map() = ExternalLinkDataItem(label,url)

fun BaseEntryDto.map(): DataItem {
    return when (this) {
        is SectionEntryDto -> map()
        is NodeEntryDto -> map()
        is LinkEntryDto -> map()
        else -> (this as ExternalLinkEntryDto ).map()
    }
}


fun List<BaseEntryDto>.mapToItemList(): List<DataItem> =
    map { it.map() }

