package com.yassine.chanlenge.network.service

import retrofit2.Response

data class ApiResponse<out T>(val status: ResponseStatus, val data: T?, val errorMessage: String?) {
    companion object {
        suspend fun <T> with(
            retrofitCall: suspend () -> Response<T>
        ): ApiResponse<T> {
            return try {
                val response = retrofitCall()
                if (response.isSuccessful) {
                    ( response.body())
                    success(response.body())
                } else {
                    error(response.errorBody()?.string())
                }

            } catch (exception: Exception) {
                exception.printStackTrace()
                error(exception.message)
            }
        }

        fun <T> success(data: T?): ApiResponse<T> {
            return ApiResponse(ResponseStatus.SUCCESS, data, null)
        }

        fun <T> error(errorMessage: String?): ApiResponse<T> {
            return ApiResponse(ResponseStatus.ERROR, null, errorMessage)
        }
    }

    val isSucces = status == ResponseStatus.SUCCESS
    val isError = status == ResponseStatus.ERROR
}

enum class ResponseStatus {
    SUCCESS,
    ERROR
}