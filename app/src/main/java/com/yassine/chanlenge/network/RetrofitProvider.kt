package com.yassine.chanlenge.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import com.yassine.chanlenge.BuildConfig
import com.yassine.chanlenge.network.dto.*
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class RetrofitProvider {

    companion object {
        const val LABEL_KEY = "type"
        const val X_API_KEY = "X-API-Key"
    }

    fun <S> createService(serviceClass: Class<S>): S {

        val moshi = Moshi.Builder()
            .add(
                PolymorphicJsonAdapterFactory.of(BaseEntryDto::class.java, LABEL_KEY)
                    .withSubtype(
                        SectionEntryDto::class.java,
                        ActionType.SECTION.columnName
                    )
                    .withSubtype(
                        NodeEntryDto::class.java,
                        ActionType.NODE.columnName
                    )
                    .withSubtype(
                        LinkEntryDto::class.java,
                        ActionType.LINK.columnName
                    )
                    .withSubtype(
                        ExternalLinkEntryDto::class.java,
                        ActionType.EXTERNAL_LINK.columnName
                    )
            )

        val builder = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi.build()))

        builder.client(getClient())
        return builder
            .build()
            .create(serviceClass)
    }

    private fun getClient(): OkHttpClient {
        val client = OkHttpClient().newBuilder()
            .retryOnConnectionFailure(true)

        client.addInterceptor(Interceptor { chain ->
            val original: Request = chain.request()
            val request = original.newBuilder()
                .header(X_API_KEY, BuildConfig.API_KEY)
                .method(original.method, original.body)
                .build()

            chain.proceed(request)
        })

        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level =
                HttpLoggingInterceptor.Level.BODY
            client.addInterceptor(httpLoggingInterceptor)
        }

        return client.build()
    }

}