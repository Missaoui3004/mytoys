package com.yassine.chanlenge.gear.repository

import com.yassine.chanlenge.network.RetrofitProvider
import com.yassine.chanlenge.network.dto.Data
import com.yassine.chanlenge.network.service.ApiResponse
import com.yassine.chanlenge.network.service.ApiService

class Repository(apiService: Class<ApiService>) {

    private val service: ApiService = RetrofitProvider().createService(apiService)


    suspend fun getAllEntries(): ApiResponse<Data> =
        ApiResponse.with {
            service.getDataEntries()
        }
}