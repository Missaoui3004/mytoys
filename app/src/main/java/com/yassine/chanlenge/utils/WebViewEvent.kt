package com.yassine.chanlenge.utils

import com.yassine.chanlenge.network.dto.ActionType

class WebViewEvent(val url: String, val type: ActionType)
class NavigationEvent(val isNavigateBack: Boolean)