package com.yassine.chanlenge.utils

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("isVisibleOrGone")
fun setVisibleOrGone(view: View, isVisible: Boolean) {
    if (isVisible) view.visibility = View.VISIBLE else view.visibility = View.GONE
}