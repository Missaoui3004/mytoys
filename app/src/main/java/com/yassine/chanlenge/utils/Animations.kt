package com.yassine.chanlenge.utils

import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.view.animation.TranslateAnimation


fun View.animate(isNavigatedBack: Boolean) {
    if (isNavigatedBack)
        startAnimation(inFromLeftAnimation())
    else
        startAnimation(inFromRightAnimation())
}


private fun inFromRightAnimation(): Animation =
    TranslateAnimation(
        Animation.RELATIVE_TO_PARENT, +1.0f,
        Animation.RELATIVE_TO_PARENT, 0.0f,
        Animation.RELATIVE_TO_PARENT, 0.0f,
        Animation.RELATIVE_TO_PARENT, 0.0f
    ).apply {
        duration = 100
        interpolator = AccelerateInterpolator()
    }


private fun inFromLeftAnimation(): Animation =
    TranslateAnimation(
        Animation.RELATIVE_TO_PARENT, -1.0f,
        Animation.RELATIVE_TO_PARENT, 0.0f,
        Animation.RELATIVE_TO_PARENT, 0.0f,
        Animation.RELATIVE_TO_PARENT, 0.0f
    ).apply {
        duration = 100
        interpolator = AccelerateInterpolator()
    }